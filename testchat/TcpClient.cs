﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Network;
using Network.Commands;
using Network.Commands.Client;
using Network.Commands.Server;
using Network.Files;
using LoadFileCommand = Network.Commands.Server.LoadFileCommand;
using FileServer;
using System.Linq;

namespace ChatClient
{
    public class TcpClient : ICommandFromServerExecutor
    {
        private TcpConnection _tcpConnection;
        private string _clientName;
        public Guid ID { get; private set; }
        private MessageHandler _messageHandler;
        public Dictionary<Guid, string> UsersOnline { get; private set; }
        public Dictionary<Guid, string> UsersHistory { get; private set; }
        public Dictionary<Guid, AttachedFilesInfo> AttachedFiles { get; private set; }
        public Dictionary<Guid, FileServer.FileInfo> ExistingFiles { get; private set; }
        public Dictionary<Guid, FileServer.FileInfo> LoadingFiles { get; set; }
        private Dictionary<Guid, Guid> PendingFileInfoIDToMessageID { get; set; }

        public event EventHandler ServerDisconnectedEvent;
        public event EventHandler ConnectionFailedEvent;
        public event EventHandler<ClientUserInfoEventArgs> UserConnectedEvent;
        public event EventHandler<ClientUserInfoEventArgs> UserDisconnectedEvent;
        public event EventHandler<MessageEventArgs> MessageReceivedEvent;
        public event EventHandler<MessageEventArgs> HistoryReceivedEvent;
        public event EventHandler UsersOnlineReceivedEvent;

        public event EventHandler<FileAddResultEventArgs> AttachFileResultEvent;
        public event EventHandler FileInfoReceivedEvent;
        public delegate void Callback();

        public void AddFile(Guid chatID, string fileName)
        {
            var fileAddResultEvent = AttachFileResultEvent;
            if (FileHelper.IsInvalidFileExtension(fileName))
            {
                fileAddResultEvent?.Invoke(this, new FileAddResultEventArgs(AttachFileResult.InvalidExtension));
                return;
            }

            var fileInfo = new System.IO.FileInfo(fileName);
            long fileSize = fileInfo.Length;
            if (FileHelper.IsInvalidSize(fileSize))
            {
                fileAddResultEvent?.Invoke(this, new FileAddResultEventArgs(AttachFileResult.FileSizeLimit));
                return;
            }

            var currentTotalFileSize = AttachedFiles[chatID].TotalSize;
            if (FileHelper.IsInvalidTotalSize(currentTotalFileSize + fileSize))
            {
                fileAddResultEvent?.Invoke(this, new FileAddResultEventArgs(AttachFileResult.TotalFileSizeLimit));
                return;
            }

            var fileStream = new FileStream(fileName, FileMode.Open);
            var memoryStream = new MemoryStream();
            fileStream.CopyTo(memoryStream);
            fileStream.Dispose();
            var uploadFileName = fileName.Split('\\').Last();
            var loadingToken = Guid.NewGuid();
            var loadingFileInfo = new FileServer.FileInfo(chatID, uploadFileName, fileSize);
            LoadingFiles.Add(loadingToken, loadingFileInfo);
            SendCommand(new LoadFileCommand(loadingToken, uploadFileName, memoryStream));
        }

        public void Start(IPEndPoint serverEndPoint, string clientName)
        {
            _clientName = clientName;
            UsersOnline = new Dictionary<Guid, string>();
            UsersHistory = new Dictionary<Guid, string>();
            AttachedFiles = new Dictionary<Guid, AttachedFilesInfo>();
            ExistingFiles = new Dictionary<Guid, FileServer.FileInfo>();
            LoadingFiles = new Dictionary<Guid, FileServer.FileInfo>();
            PendingFileInfoIDToMessageID = new Dictionary<Guid, Guid>();
            _tcpConnection = new TcpConnection(serverEndPoint);
            _tcpConnection.ConnectionCreateEvent += OnConnectionCreate;
            _tcpConnection.ReceiveEvent += OnReceiveCommand;
            _tcpConnection.ConnectionLostEvent += OnServerDisconnect;
            _tcpConnection.Start();

            _messageHandler = new MessageHandler();
            _messageHandler.MessageAddedEvent += OnMessageAdded;
            _messageHandler.HistoryAddedEvent += OnHistoryAdded;
        }

        public void Stop()
        {
            if (_tcpConnection.Connected)
            {
                SendCommand(new DisconnectCommand(ID));
            }
        }

        public IEnumerable<Message> GetMessages(Guid receiverID)
        {
            return _messageHandler.GetMessages(ID, receiverID);
        }

        private void OnHistoryAdded(object sender, MessageEventArgs e)
        {
            var message = _messageHandler.GetMessage(e.MessageID);
            foreach (var attachedFileID in message.AttachedFilesIDs)
            {
                SendCommand(new Network.Commands.Server.GetFileInfoCommand(attachedFileID));
                PendingFileInfoIDToMessageID.Add(attachedFileID, e.MessageID);
            }
            var historyReceivedEvent = HistoryReceivedEvent;
            historyReceivedEvent?.Invoke(this, e);
        }

        private void OnMessageAdded(object sender, MessageEventArgs e)
        {
            var message = _messageHandler.GetMessage(e.MessageID);
            foreach (var attachedFileID in message.AttachedFilesIDs)
            {
                SendCommand(new Network.Commands.Server.GetFileInfoCommand(attachedFileID));
                PendingFileInfoIDToMessageID.Add(attachedFileID, e.MessageID);
            }
            var messageReceivedEvent = MessageReceivedEvent;
            messageReceivedEvent?.Invoke(this, e);
        }

        public Message GetMessage(Guid messageID)
        {
            return _messageHandler.GetMessage(messageID);
        }

        private void OnServerDisconnect(object sender, EventArgs e)
        {
            var serverDisconnectedEvent = ServerDisconnectedEvent;
            serverDisconnectedEvent?.Invoke(this, EventArgs.Empty);
        }

        public void OnReceiveMessageHistoryCommand(Message message)
        {
            _messageHandler.AddHistory(message);
        }

        public void OnSetUserIDCommand(Guid id)
        {
            if (id == SetUserIDCommand.InvalidID)
            {
                _tcpConnection.OnConnectionLost();
                var connectionFailedEvent = ConnectionFailedEvent;
                connectionFailedEvent?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                ID = id;
            }
        }

        public void SendMessage(Guid receiverID, string messageText)
        {
            var attachedFileIDsList = new List<Guid>();
            foreach (var attachedFileID in AttachedFiles[receiverID].AttachedFiles.Keys)
            {
                attachedFileIDsList.Add(attachedFileID);
            }
            SendCommand(new SendMessageCommand(ID, receiverID, messageText, attachedFileIDsList));
            AttachedFiles[receiverID].AttachedFiles.Clear();
        }

        public void OnUserConnectedCommand(Guid userID, string name)
        {
            UsersOnline.Add(userID, name);
            UsersHistory.Add(userID, name);
            AttachedFiles.Add(userID, new AttachedFilesInfo());
            var userConnectedEvent = UserConnectedEvent;
            var eventArgs = new ClientUserInfoEventArgs(userID, name);
            userConnectedEvent?.Invoke(this, eventArgs);
        }

        public void OnUserDisconnectedCommand(Guid userID)
        {
            var userDisconnectedEvent = UserDisconnectedEvent;
            var eventArgs = new ClientUserInfoEventArgs(userID, UsersOnline[userID]);
            userDisconnectedEvent?.Invoke(this, eventArgs);
            UsersOnline.Remove(userID);
            AttachedFiles.Remove(userID);
        }

        private void OnConnectionCreate(object sender, EventArgs eventArgs)
        {
            SendCommand(new GetIDCommand(_clientName));
        }

        private void OnReceiveCommand(object sender, ByteArrayEventArgs eventArgs)
        {
            var stringCommand = Encoding.Unicode.GetString(eventArgs.ByteArray);
            var command = CommandFromServerFactory.GetCommand(stringCommand);
            command.Execute(this);
        }

        private void SendCommand(BaseCommand command)
        {
            var stringCommand = command.ToString();
            _tcpConnection.SendMessage(Encoding.Unicode.GetBytes(stringCommand));
        }

        public void OnReceiveUsersOnlineCommand(Dictionary<Guid, string> usersOnline)
        {
            foreach (var user in usersOnline)
            {
                UsersOnline.Add(user.Key, user.Value);
                AttachedFiles.Add(user.Key, new AttachedFilesInfo());
            }
            var usersOnlineReceivedEvent = UsersOnlineReceivedEvent;
            usersOnlineReceivedEvent?.Invoke(this, EventArgs.Empty);
        }

        public void OnReceiveMessageCommand(Message message)
        {
            _messageHandler.AddMessage(message);
        }

        public void OnReceiveUsersHistoryCommand(Dictionary<Guid, string> usersHistory)
        {
            foreach (var user in usersHistory)
            {
                UsersHistory.Add(user.Key, user.Value);
            }
        }

        public void OnDeleteFileCommand(Guid fileID)
        {

        }

        public void OnGetFileCommand(Guid fileID, string fileBase64)
        {
            var fileName = ExistingFiles[fileID].Name;
            DownloadManager.SaveFile(fileID, fileName, fileBase64);
        }

        public void OnGetFileInfoCommand(FileServer.FileInfo fileInfo)
        {
            var messageID = PendingFileInfoIDToMessageID[fileInfo.ID];
            ExistingFiles.Add(fileInfo.ID, fileInfo);
            var fileInfoReceivedEvent = FileInfoReceivedEvent;
            fileInfoReceivedEvent?.Invoke(this, EventArgs.Empty);
        }

        public void OnLoadFileCommand(Guid loadingToken, Guid fileID)
        {
            var loadingFileInfo = LoadingFiles[loadingToken];
            var fileInfo = new FileServer.FileInfo(fileID, loadingFileInfo.Name, loadingFileInfo.Size);
            LoadingFiles.Remove(loadingToken);
            AttachedFiles[loadingFileInfo.ID].AttachedFiles.Add(fileInfo.ID, fileInfo);
            var fileAddResultEvent = AttachFileResultEvent;
            fileAddResultEvent?.Invoke(this, new FileAddResultEventArgs(AttachFileResult.Ok));
        }

        public void UnattachFile(Guid dialogID, Guid fileID)
        {
            SendCommand(new Network.Commands.Server.DeleteFileCommand(fileID));
            AttachedFiles[dialogID].AttachedFiles.Remove(fileID);
            var fileAddResultEvent = AttachFileResultEvent;
            fileAddResultEvent?.Invoke(this, new FileAddResultEventArgs(AttachFileResult.Ok));
        }

        public void HandleFileClick(Guid fileID)
        {
            var fileStatus = DownloadManager.GetFileStatus(fileID);
            switch (fileStatus)
            {
                case FileStatus.Downloaded:
                    DownloadManager.OpenFile(fileID);
                    break;
                case FileStatus.NotDownloaded:
                    SendCommand(new Network.Commands.Server.GetFileCommand(fileID));
                    break;
            }
        }
    }
}
