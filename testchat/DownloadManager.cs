﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    public static class DownloadManager
    {
        private const string DownloadFolderName = "ChatDownloads";
        private static string MyDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        private static string DownloadsFolderPath = Path.Combine(MyDocumentsPath, DownloadFolderName);

        private static List<Guid> DownloadingFiles = new List<Guid>();
        private static Dictionary<Guid, DownloadedFileInfo> DownloadedFiles = new Dictionary<Guid, DownloadedFileInfo>();

        public static event EventHandler<DownloadStatusChangedEventArgs> DownloadStatusChangedEvent;

        static DownloadManager()
        {
            if (!Directory.Exists(DownloadsFolderPath))
            {
                Directory.CreateDirectory(DownloadsFolderPath);
            }
        }
        
        public static void SaveFile(Guid fileID, string fileName, string fileBase64)
        {
            var downloadStatusChangedEvent = DownloadStatusChangedEvent;
            var downloadStatusChangedEventArgs = new DownloadStatusChangedEventArgs(fileID);
            if (!DownloadedFiles.ContainsKey(fileID))
            {
                DownloadingFiles.Add(fileID);
                downloadStatusChangedEvent?.Invoke(null, downloadStatusChangedEventArgs);

                var bytes = Convert.FromBase64String(fileBase64);
                var fileSize = bytes.Length;

                var fullPath = GetFullPathWithIndex(fileName);
                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    fileStream.Write(bytes, 0, bytes.Length);
                }

                var downloadedFileInfo = new DownloadedFileInfo(fileID, fileName, fileSize, fullPath);
                DownloadingFiles.Remove(fileID);
                DownloadedFiles.Add(fileID, downloadedFileInfo);
                downloadStatusChangedEvent?.Invoke(null, downloadStatusChangedEventArgs);
            }
        }

        public static FileStatus GetFileStatus(Guid fileID)
        {
            if (DownloadingFiles.Contains(fileID))
            {
                return FileStatus.Downloading;
            }
            if (DownloadedFiles.ContainsKey(fileID))
            {
                return FileStatus.Downloaded;
            }
            return FileStatus.NotDownloaded;
        }

        public static void OpenFile(Guid fileID)
        {
            System.Diagnostics.Process.Start(DownloadedFiles[fileID].FullPath);
        }

        private static string GetFullPathWithIndex(string fileName)
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var extension = Path.GetExtension(fileName);
            var fullPath = Path.Combine(DownloadsFolderPath, fileName);
            string fileNameWithIndex;
            int count = 0;
            while (File.Exists(fullPath))
            {
                count++;
                fileNameWithIndex = fileNameWithoutExtension + " (" + count.ToString() + ")" + extension;
                fullPath = Path.Combine(DownloadsFolderPath, fileNameWithIndex);
            }
            return fullPath;
        }
    }
}
