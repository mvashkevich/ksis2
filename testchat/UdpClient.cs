﻿using System.Net;
using System.Net.Sockets;
using Network;
using Server;

namespace ChatClient
{
    public static class UdpClient
    {
        private const int BufferSize = 1024;
        private const int TimeOut = 1000;
        private static byte[] udpReceiveBuffer = new byte[BufferSize];

        public static IPEndPoint GetServerEndPoint()
        {
            var udpClientSocket = CreateUdpSocket();
            SendLocalEndPointByBroadcast(udpClientSocket);
            var serverEndPoint = ReceiveServerEndPoint(udpClientSocket);
            return serverEndPoint;
        }

        private static IPEndPoint ReceiveServerEndPoint(Socket udpClientSocket)
        {
            IPEndPoint serverEndPoint = null;
            var senderIPEndPoint = new IPEndPoint(IPAddress.None, 0);
            EndPoint senderEndPoint = senderIPEndPoint;
            int bytesCount = 0;
            try
            {
                bytesCount = udpClientSocket.ReceiveFrom(udpReceiveBuffer, ref senderEndPoint);
            }
            catch (SocketException e)
            {
                switch (e.SocketErrorCode)
                {
                    case SocketError.TimedOut:
                        {
                            break;
                        }
                    default:
                        {
                            throw e;
                        }
                }
            }
            if (bytesCount > 0)
            {
                serverEndPoint = BinaryEndPoint.DeserializeEndPoint(udpReceiveBuffer, bytesCount);
            }
            return serverEndPoint;
        }

        private static void SendLocalEndPointByBroadcast(Socket udpClientSocket)
        {
            var broadcastEndPoint = new IPEndPoint(IPAddress.Broadcast, UdpServer.UdpBroadcastPort);
            udpClientSocket.SendTo(BinaryEndPoint.SerializeEndPoint((IPEndPoint)udpClientSocket.LocalEndPoint), broadcastEndPoint);
        }

        private static Socket CreateUdpSocket()
        {
            var clientUdpEndPoint = new IPEndPoint(NetworkHelper.GetLocalIPv4Address(), 0);
            var udpClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpClientSocket.EnableBroadcast = true;
            udpClientSocket.ReceiveTimeout = TimeOut;
            udpClientSocket.Bind(clientUdpEndPoint);
            return udpClientSocket;
        }
    }
}
