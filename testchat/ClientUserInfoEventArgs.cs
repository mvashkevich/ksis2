﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    public class ClientUserInfoEventArgs : EventArgs
    {
        public Guid ID { get; }
        public string Name { get; }
        public ClientUserInfoEventArgs(Guid id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}
