﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileServer;

namespace ChatClient
{
    class DownloadedFileInfo : FileInfo
    {
        public string FullPath { get; private set; }

        public DownloadedFileInfo(Guid fileID, string fileName, long fileSize, string fullPath) : base(fileID, fileName, fileSize)
        {
            FullPath = fullPath;
        }
    }
}
