﻿using System;

namespace ChatClient
{
    public class FileInfoReceivedEventArgs : EventArgs
    {
        public Guid MessageID { get; private set; }
        public Guid AttachedFileID { get; private set; }

        public FileInfoReceivedEventArgs(Guid messageID, Guid attachedFileID)
        {
            MessageID = messageID;
            AttachedFileID = attachedFileID;
        }
    }
}