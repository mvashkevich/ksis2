﻿using System;

namespace ChatClient
{
    public class DownloadStatusChangedEventArgs : EventArgs
    {
        public Guid FileID { get; private set; }

        public DownloadStatusChangedEventArgs(Guid fileID)
        {
            FileID = fileID;
        }
    }
}