﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    public class AttachedFilesInfo
    {
        public long TotalSize
        {
            get
            {
                long totalSize = 0;
                foreach (var fileInfo in AttachedFiles.Values)
                {
                    totalSize += fileInfo.Size;
                }
                return totalSize;
            }
        }
        public Dictionary<Guid, FileServer.FileInfo> AttachedFiles = new Dictionary<Guid, FileServer.FileInfo>();
    }
}
