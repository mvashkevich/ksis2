﻿using System;
using System.Net;
using Network;

namespace ChatClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();
            program.Run();
        }
        public void Run()
        {
            IPEndPoint ipEndPoint;
            do
            {
                ipEndPoint = UdpClient.GetServerEndPoint();
                if (ipEndPoint == null)
                {
                    Console.WriteLine("Server not found. Retry? (Y/N)");
                    if (Console.ReadKey().Key != ConsoleKey.Y)
                    {
                        return;
                    }
                }
            }
            while (ipEndPoint == null);
            var client = new TcpClient();
            client.ServerDisconnectedEvent += OnServerDisconnectedEvent;
            client.UserDisconnectedEvent += Client_UserDisconnectedEvent;
            client.UserConnectedEvent += Client_UserConnectedEvent;
            client.MessageReceivedEvent += Client_MessageReceivedEvent;
            client.HistoryReceivedEvent += Client_HistoryReceivedEvent;
            Console.WriteLine("Enter name:");
            string clientName = Console.ReadLine();
            client.Start(ipEndPoint, clientName);
            Console.Read();
            Guid user = Guid.Empty;
            foreach (var u in client.UsersOnline)
            {
                if (u.Value == "Common chat")
                {
                    user = u.Key;
                }
            }
            while (true)
            {
                var message = Console.ReadLine();
                client.SendMessage(user, message);
            }
        }

        private void Client_HistoryReceivedEvent(object sender, Network.MessageEventArgs e)
        {
            var client = ((TcpClient)sender);
            var rec = client.GetMessage(e.MessageID).ReceiverID;
            Console.WriteLine("Hist");
            foreach (var message in client.GetMessages(rec))
            {
                PrintMessage(sender, message);
            }
            Console.WriteLine("Tsih");
        }

        private void Client_MessageReceivedEvent(object sender, Network.MessageEventArgs e)
        {
            var message = ((TcpClient)sender).GetMessage(e.MessageID);
            PrintMessage(sender, message);
        }

        private static void PrintMessage(object sender, Message message)
        {
            var senderName = ((TcpClient)sender).UsersHistory[message.SenderID];
            Console.WriteLine(senderName + ": " + message.MessageText + " " + message.DateTime.ToString("f"));
        }

        private void Client_UserDisconnectedEvent(object sender, ClientUserInfoEventArgs e)
        {
            Console.WriteLine("disc u " + e.Name + e.ID);


        }

        private void Client_UserConnectedEvent(object sender, ClientUserInfoEventArgs e)
        {
            Console.WriteLine("new u " + e.Name + e.ID);
        }

        private void OnServerDisconnectedEvent(object sender, EventArgs e)
        {
            Console.WriteLine("Server disconnected. Retry? (Y/N)");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Run();
            }
        }
    }
}
