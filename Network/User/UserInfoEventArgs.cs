﻿using System;

namespace Network.User
{
    public class UserInfoEventArgs : EventArgs
    {
        public UserInfo UserInfo { get; private set; }

        public UserInfoEventArgs(UserInfo userInfo)
        {
            UserInfo = userInfo;
        }
    }
}
