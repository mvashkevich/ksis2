﻿using System;

namespace Network.User
{
    public class UserInfo
    {
        public Guid ID { get; private set; }
        public string Name { get; private set; }
        public TcpConnection Connection { get; private set; }
        public UserInfo(Guid id, string name, TcpConnection connection) { ID = id; Name = name; Connection = connection; }
        public override string ToString()
        {
            return ID + " " + Name;
        }
    }
}
