﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Network;

namespace Network
{
    public class MessageHandler
    {
        private const string _commonChatGuidString = "acd3f938-b4f1-4ea0-96c0-966f5685b680";
        public static readonly Guid CommonChatGuid = new Guid(_commonChatGuidString);
        private Dictionary<Guid, Message> _messages = new Dictionary<Guid, Message>();

        public event EventHandler<MessageEventArgs> MessageAddedEvent;
        public event EventHandler<MessageEventArgs> HistoryAddedEvent;

        public void AddMessage(Guid senderID, Guid receiverID, string messageText, List<Guid> attachedFileIDs)
        {
            var messageID = Guid.NewGuid();
            var message = new Message(senderID, receiverID, messageText, attachedFileIDs);
            _messages.Add(messageID, message);
            var onAddMessage = MessageAddedEvent;
            onAddMessage?.Invoke(this, new MessageEventArgs(messageID));
        }
        public void AddMessage(Message message)
        {
            var messageID = Guid.NewGuid();
            _messages.Add(messageID, message);
            var onAddMessage = MessageAddedEvent;
            onAddMessage?.Invoke(this, new MessageEventArgs(messageID));
        }
        public void AddHistory(Message message)
        {
            var messageID = Guid.NewGuid();
            _messages.Add(messageID, message);
            var historyAddedEvent = HistoryAddedEvent;
            historyAddedEvent?.Invoke(this, new MessageEventArgs(messageID));
        }
        public IEnumerable<Message> GetMessages(Guid senderID, Guid receiverID)
        {
            if (receiverID == CommonChatGuid)
            {
                return from message in _messages.Values where (message.ReceiverID == CommonChatGuid) orderby message.DateTime select message;

            }
            return from message in _messages.Values where ((message.ReceiverID == receiverID && message.SenderID == senderID) || (message.ReceiverID == senderID && message.SenderID == receiverID)) orderby message.DateTime select message;
        }
        public Message GetMessage(Guid messageID)
        {
            return _messages[messageID];
        }
    }
}
