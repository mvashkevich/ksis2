﻿namespace ChatClient
{
    public enum AttachFileResult
    {
        Ok, InvalidExtension, FileSizeLimit, TotalFileSizeLimit
    }
}