﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Files
{
    public static class FileHelper
    {
        public static readonly string[] UnacceptableExtensions = { ".exe", ".com", ".bat" };
        public static long MaxFileSize { get; } = 20 * 1024 * 1024;
        public static long MaxTotalSize { get; } = 50 * 1024 * 1024;

        public static bool IsInvalidFileExtension(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            return UnacceptableExtensions.Contains(extension);
        }

        public static bool IsInvalidSize(long size)
        {
            return size > MaxFileSize;
        }

        public static bool IsInvalidTotalSize(long totalSize)
        {
            return totalSize > MaxTotalSize;
        }
    }
}
