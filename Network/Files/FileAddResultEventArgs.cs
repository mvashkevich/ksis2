﻿using System;

namespace ChatClient
{
    public class FileAddResultEventArgs : EventArgs
    {
        public AttachFileResult Result { get; private set; }

        public FileAddResultEventArgs(AttachFileResult result)
        {
            Result = result;
        }
    }
}