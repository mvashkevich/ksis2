﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    public class MessageEventArgs : EventArgs
    {
        public Guid MessageID { get; private set; }

        public MessageEventArgs(Guid messageID)
        {
            MessageID = messageID;
        }
    }
}
