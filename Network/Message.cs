﻿using System;
using System.Collections.Generic;

namespace Network
{
    public class Message
    {
        public Guid SenderID { get; private set; }
        public Guid ReceiverID { get; private set; }
        public DateTime DateTime { get; private set; }
        public string MessageText { get; private set; }
        public List<Guid> AttachedFilesIDs { get; private set; }
        public Message(Guid senderID, Guid receiverID, string messageText)
        {
            SenderID = senderID;
            ReceiverID = receiverID;
            DateTime = DateTime.Now;
            MessageText = messageText;
        }
        public Message(Guid senderID, Guid receiverID, DateTime dateTime, string messageText)
        {
            SenderID = senderID;
            ReceiverID = receiverID;
            DateTime = dateTime;
            MessageText = messageText;
        }
        public Message(Guid senderID, Guid receiverID, string messageText, List<Guid> attachedFilesIDs) : this(senderID, receiverID, messageText)
        {
            AttachedFilesIDs = attachedFilesIDs;
        }
        public Message(Guid senderID, Guid receiverID, DateTime dateTime, string messageText, List<Guid> attachedFilesIDs) : this(senderID, receiverID, dateTime, messageText)
        {
            AttachedFilesIDs = attachedFilesIDs;
        }
        public override string ToString()
        {
            return SenderID + "->" + ReceiverID + ":" + DateTime.ToString() + ":" + MessageText;
        }
    }
}
