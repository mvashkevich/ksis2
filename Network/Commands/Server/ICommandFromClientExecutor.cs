﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Network.Commands.Server
{
    public interface ICommandFromClientExecutor
    {
        void OnGetIDCommand(TcpConnection connection, string name);
        void OnSendMessageCommand(TcpConnection connection, Guid senderID, Guid receiverID, string message, List<Guid> attachedFileIDs);
        void OnDisconnectCommand(TcpConnection connection, Guid senderID);
        Task OnLoadFile(TcpConnection connection, Guid loadingToken, string fileName, Stream fileStream);
        Task OnDeleteFile(TcpConnection connection, Guid fileID);
        Task OnGetFile(TcpConnection connection, Guid fileID);
        Task OnGetFileInfo(TcpConnection connection, Guid fileID);
    }
}
