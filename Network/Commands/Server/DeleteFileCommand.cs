﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Server
{
    public class DeleteFileCommand : BaseFileIDCommand
    {
        internal const string Type = "DLTF";

        public DeleteFileCommand(string arguments) : base(arguments) { }

        public DeleteFileCommand(Guid fileID) : base(fileID) { }

        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnDeleteFile(connection, _fileID);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileID.ToString();
        }
    }
}
