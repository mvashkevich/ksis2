﻿using System;

namespace Network.Commands.Server
{
    public static class CommandFromClientFactory
    {
        public static BaseCommandFromClient GetCommand(string commandText)
        {
            if (commandText == null)
            {
                throw new ArgumentNullException(nameof(commandText));
            }
            int delimiterIndex = commandText.IndexOf(BaseCommand.CommandArgumentsDelimiter, StringComparison.Ordinal);
            string commandType = commandText.Substring(0, delimiterIndex);
            string commandArguments = commandText.Substring(delimiterIndex + 1);

            switch (commandType)
            {
                case GetIDCommand.Type:
                    {
                        return new GetIDCommand(commandArguments);
                    }
                case SendMessageCommand.Type:
                    {
                        return new SendMessageCommand(commandArguments);
                    }
                case DisconnectCommand.Type:
                    {
                        return new DisconnectCommand(commandArguments);
                    }
                case LoadFileCommand.Type:
                    {
                        return new LoadFileCommand(commandArguments);
                    }
                case GetFileCommand.Type:
                    {
                        return new GetFileCommand(commandArguments);
                    }
                case GetFileInfoCommand.Type:
                    {
                        return new GetFileInfoCommand(commandArguments);
                    }
                case DeleteFileCommand.Type:
                    {
                        return new DeleteFileCommand(commandArguments);
                    }
            }
            throw new ArgumentException("Invalid command type: " + commandType);
        }
    }
}
