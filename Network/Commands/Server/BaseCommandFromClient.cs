﻿namespace Network.Commands.Server
{
    public abstract class BaseCommandFromClient : BaseCommand
    {
        public abstract void Execute(ICommandFromClientExecutor executor, TcpConnection connection);
    }
}
