﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Server
{
    public class GetFileInfoCommand : BaseFileIDCommand
    {
        internal const string Type = "GFII";

        public GetFileInfoCommand(string arguments) : base(arguments) { }

        public GetFileInfoCommand(Guid fileID) : base(fileID) { }

        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnGetFileInfo(connection, _fileID);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileID.ToString();
        }
    }
}
