﻿using System;

namespace Network.Commands.Server
{
    public class DisconnectCommand : BaseCommandFromClient
    {
        internal const string Type = "QUIT";
        private readonly Guid _senderID;

        public DisconnectCommand(Guid senderId)
        {
            _senderID = senderId;
        }
        public DisconnectCommand(string arguments)
        {
            _senderID = Guid.Parse(arguments);
        }
        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnDisconnectCommand(connection, _senderID);
        }
        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _senderID;
        }
    }
}
