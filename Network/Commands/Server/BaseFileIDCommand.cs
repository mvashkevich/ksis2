﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Server
{
    public abstract class BaseFileIDCommand : BaseCommandFromClient
    {
        protected Guid _fileID;

        public BaseFileIDCommand(string arguments)
        {
            _fileID = Guid.Parse(arguments);
        }

        public BaseFileIDCommand(Guid fileID)
        {
            _fileID = fileID;
        }
    }
}
