﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Server
{
    public class LoadFileCommand : BaseCommandFromClient, IDisposable
    {
        internal const string Type = "LFIL";
        private const int ArgumentsCount = 3;
        private const int LoadingTokenIndex = 0;
        private const int FileNameIndex = 1;
        private const int FileStreamIndex = 2;
        private Guid _loadingToken;
        private string _fileName;
        private MemoryStream _fileStream;

        public LoadFileCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray(), ArgumentsCount);
            _loadingToken = Guid.Parse(splitArguments[LoadingTokenIndex]);
            _fileName = splitArguments[FileNameIndex];
            var fileStreamBytes = Convert.FromBase64String(splitArguments[FileStreamIndex]);
            _fileStream = new MemoryStream(fileStreamBytes);
        }

        public LoadFileCommand(Guid loadingToken, string fileName, MemoryStream fileStream)
        {
            _loadingToken = loadingToken;
            _fileName = fileName;
            _fileStream = fileStream;
        }

        public override string ToString()
        {
            string fileStreamInBase64;
            _fileStream.Position = 0;
            fileStreamInBase64 = Convert.ToBase64String(_fileStream.ToArray());
            return Type + CommandArgumentsDelimiter + _loadingToken.ToString() 
                + CommandArgumentsDelimiter +  _fileName
                + CommandArgumentsDelimiter + fileStreamInBase64;
        }
        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnLoadFile(connection, _loadingToken, _fileName, _fileStream);
        }

        public void Dispose()
        {
            _fileStream?.Dispose();
        }
    }
}
