﻿namespace Network.Commands.Server
{
    public class GetIDCommand : BaseCommandFromClient
    {
        internal const string Type = "GTID";
        private readonly string _name;

        public GetIDCommand(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _name;
        }

        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnGetIDCommand(connection, _name);
        }
    }
}
