﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Server
{
    public class GetFileCommand : BaseFileIDCommand
    {
        internal const string Type = "GFIL";

        public GetFileCommand(string arguments) : base(arguments) { }

        public GetFileCommand(Guid fileID) : base(fileID) { }

        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnGetFile(connection, _fileID);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileID;
        }
    }
}
