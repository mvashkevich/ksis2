﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Network.Commands.Server
{
    public class SendMessageCommand : BaseCommandFromClient
    {
        internal const string Type = "SEND";
        private const int ConstArgumentsCount = 4;
        private const int SenderIDIndex = 0;
        private const int ReceiverIDIndex = 1;
        private const int AttachedFileIDsCountIndex = 2;
        private const int RestIndex = 3;
        private readonly Guid _senderID;
        private readonly Guid _receiverID;
        private readonly string _message;
        private List<Guid> _attachedFileIDs;

        public SendMessageCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray(), ConstArgumentsCount);
            _senderID = Guid.Parse(splitArguments[SenderIDIndex]);
            _receiverID = Guid.Parse(splitArguments[ReceiverIDIndex]);
            var attachedFileIDsCount = int.Parse(splitArguments[AttachedFileIDsCountIndex]);
            var fileIDsAndMessageCount = attachedFileIDsCount + 1;
            var splitFileIDsAndMessage = splitArguments[RestIndex].Split(CommandArgumentsDelimiter.ToCharArray(), fileIDsAndMessageCount);
            _attachedFileIDs = new List<Guid>();
            for (int i = 0; i < attachedFileIDsCount; i++)
            {
                _attachedFileIDs.Add(Guid.Parse(splitFileIDsAndMessage[i]));
            }
            var messageIndex = splitFileIDsAndMessage.Length - 1;
            _message = splitFileIDsAndMessage[messageIndex];
        }

        public SendMessageCommand(Guid senderID, Guid receiverID, string message, List<Guid> attachedFileIDs)
        {
            _senderID = senderID;
            _receiverID = receiverID;
            _message = message;
            _attachedFileIDs = attachedFileIDs;
        }

        public override void Execute(ICommandFromClientExecutor executor, TcpConnection connection)
        {
            executor?.OnSendMessageCommand(connection, _senderID, _receiverID, _message, _attachedFileIDs);
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder(Type + CommandArgumentsDelimiter + _senderID
                + CommandArgumentsDelimiter + _receiverID + CommandArgumentsDelimiter + _attachedFileIDs.Count);
            foreach(var attachedFileID in _attachedFileIDs)
            {
                stringBuilder.Append(CommandArgumentsDelimiter + attachedFileID);
            }
            stringBuilder.Append(CommandArgumentsDelimiter + _message);
            return stringBuilder.ToString();
        }
    }
}
