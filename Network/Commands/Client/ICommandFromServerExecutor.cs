﻿using System;
using System.Collections.Generic;
using System.IO;
using FileInfo = FileServer.FileInfo;

namespace Network.Commands.Client
{
    public interface ICommandFromServerExecutor
    {
        void OnSetUserIDCommand(Guid id);
        void OnReceiveMessageHistoryCommand(Message message);
        void OnReceiveMessageCommand(Message message);
        void OnReceiveUsersOnlineCommand(Dictionary<Guid, string> usersOnline);
        void OnReceiveUsersHistoryCommand(Dictionary<Guid, string> usersOnline);
        void OnUserDisconnectedCommand(Guid userID);
        void OnUserConnectedCommand(Guid userID, string name);
        void OnLoadFileCommand(Guid loadingToken, Guid fileID);
        void OnGetFileCommand(Guid fileID, string fileBase64);
        void OnGetFileInfoCommand(FileInfo fileInfo);
        void OnDeleteFileCommand(Guid fileID);
    }
}
