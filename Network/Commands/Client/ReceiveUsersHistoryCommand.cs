﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Client
{
    public class ReceiveUsersHistoryCommand : BaseUsersCommand
    {
        internal const string Type = "USRH";

        public ReceiveUsersHistoryCommand(string arguments) : base(arguments) { }

        public ReceiveUsersHistoryCommand(IReadOnlyDictionary<Guid, string> users) : base(users) { }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnReceiveUsersHistoryCommand(Users);
        }

        public override string ToString()
        {
            return Type + base.ToString();
        }
    }
}
