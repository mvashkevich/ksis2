﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Network.Commands.Client
{
    public abstract class BaseMessageCommand : BaseCommandFromServer
    {
        private const string DateFormat = "o";
        private const int ArgumentsCount = 5;
        private const int SenderIDIndex = 0;
        private const int ReceiverIDIndex = 1;
        private const int DateTimeIndex = 2;
        private const int AttachedFileIDsCountIndex = 3;
        private const int RestIndex = 4;
        protected Message Message { get; private set; }

        public BaseMessageCommand(Message message)
        {
            Message = message;
        }
        public BaseMessageCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray(), ArgumentsCount);
            var senderID = Guid.Parse(splitArguments[SenderIDIndex]);
            var receiverID = Guid.Parse(splitArguments[ReceiverIDIndex]);
            var dateTime = DateTime.Parse(splitArguments[DateTimeIndex], CultureInfo.InvariantCulture);
            var attachedFileIDsCount = int.Parse(splitArguments[AttachedFileIDsCountIndex]);
            var fileIDsAndMessageCount = attachedFileIDsCount + 1;
            var splitFileIDsAndMessage = splitArguments[RestIndex].Split(CommandArgumentsDelimiter.ToCharArray(), fileIDsAndMessageCount);
            var attachedFileIDs = new List<Guid>();
            for (int i = 0; i < attachedFileIDsCount; i++)
            {
                attachedFileIDs.Add(Guid.Parse(splitFileIDsAndMessage[i]));
            }
            var messageIndex = splitFileIDsAndMessage.Length - 1;
            var messageText = splitFileIDsAndMessage[messageIndex];
            Message = new Message(senderID, receiverID, dateTime, messageText, attachedFileIDs);
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder(CommandArgumentsDelimiter
                + Message.SenderID + CommandArgumentsDelimiter
                + Message.ReceiverID + CommandArgumentsDelimiter
                + Message.DateTime.ToString(DateFormat, CultureInfo.InvariantCulture)
                + CommandArgumentsDelimiter + Message.AttachedFilesIDs.Count);
            foreach (var attachedFileID in Message.AttachedFilesIDs)
            {
                stringBuilder.Append(CommandArgumentsDelimiter + attachedFileID);
            }
            stringBuilder.Append(CommandArgumentsDelimiter + Message.MessageText);
            return stringBuilder.ToString();
        }
    }
}
