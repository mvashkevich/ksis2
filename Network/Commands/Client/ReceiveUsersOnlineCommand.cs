﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Network.Commands.Client
{
    public class ReceiveUsersOnlineCommand : BaseUsersCommand
    {
        internal const string Type = "RECU";

        public ReceiveUsersOnlineCommand(string arguments) : base(arguments) { }

        public ReceiveUsersOnlineCommand(Dictionary<Guid, string> usersOnline) : base(usersOnline) { }

        public override string ToString()
        {
            return Type + base.ToString();
        }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnReceiveUsersOnlineCommand(Users);
        }
    }
}
