﻿using System;

namespace Network.Commands.Client
{
    public class SetUserIDCommand : BaseCommandFromServer
    {
        internal const string Type = "STID";
        public static readonly Guid InvalidID = Guid.Empty;
        private readonly Guid _id;

        public SetUserIDCommand(string id)
        {
            _id = Guid.Parse(id);
        }
        public SetUserIDCommand(Guid id)
        {
            _id = id;
        }
        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _id;
        }
        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnSetUserIDCommand(_id);
        }
    }
}
