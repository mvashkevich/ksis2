﻿namespace Network.Commands.Client
{
    public class ReceiveMessageHistoryCommand : BaseMessageCommand
    {
        internal const string Type = "RECH";

        public ReceiveMessageHistoryCommand(Message message) : base(message) { }

        public ReceiveMessageHistoryCommand(string arguments) : base(arguments) { }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnReceiveMessageHistoryCommand(Message);
        }

        public override string ToString()
        {
            return Type + base.ToString();
        }
    }
}
