﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Client
{
    public class DeleteFileCommand : BaseFileIDCommand
    {
        internal const string Type = "DELF";

        public DeleteFileCommand(string arguments) : base(arguments) { }

        public DeleteFileCommand(Guid fileID) : base(fileID) { }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnDeleteFileCommand(_fileID);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileID.ToString();
        }
    }
}
