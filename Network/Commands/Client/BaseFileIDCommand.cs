﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Client
{
    public abstract class BaseFileIDCommand : BaseCommandFromServer
    {
        protected Guid _fileID;

        public BaseFileIDCommand(string arguments)
        {
            _fileID = Guid.Parse(arguments);
        }

        public BaseFileIDCommand(Guid fileID)
        {
            _fileID = fileID;
        }
    }
}
