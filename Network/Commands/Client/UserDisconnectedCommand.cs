﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Client
{
    public class UserDisconnectedCommand : BaseCommandFromServer
    {
        internal const string Type = "USRD";
        private readonly Guid _id;

        public UserDisconnectedCommand(string id)
        {
            _id = Guid.Parse(id);
        }
        public UserDisconnectedCommand(Guid id)
        {
            _id = id;
        }
        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _id;
        }
        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnUserDisconnectedCommand(_id);
        }
    }
}
