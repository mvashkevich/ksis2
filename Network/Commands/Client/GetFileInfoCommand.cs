﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileServer;

namespace Network.Commands.Client
{
    public class GetFileInfoCommand : BaseCommandFromServer
    {
        internal const string Type = "GETI";
        private FileInfo _fileInfo;

        public GetFileInfoCommand(string arguments)
        {
            _fileInfo = new FileInfo(arguments);
        }

        public GetFileInfoCommand(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnGetFileInfoCommand(_fileInfo);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileInfo.ToString();
        }
    }
}
