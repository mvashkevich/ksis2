﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Client
{
    public abstract class BaseUsersCommand : BaseCommandFromServer
    {
        protected const int ArgumentsCount = 2;
        protected const int SizeIndex = 0;
        protected const int UsersOffset = 1;
        protected const int IDIndex = 0;
        protected const int NameIndex = 1;

        protected Dictionary<Guid, string> Users { get; private set; }
        public BaseUsersCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray());
            var usersCount = int.Parse(splitArguments[SizeIndex], NumberFormatInfo.InvariantInfo);
            Users = new Dictionary<Guid, string>(usersCount);
            for (int i = UsersOffset; i < splitArguments.Length; i += ArgumentsCount)
            {
                var id = Guid.Parse(splitArguments[i + IDIndex]);
                var name = splitArguments[i + NameIndex];
                Users.Add(id, name);
            }
        }

        public BaseUsersCommand(IReadOnlyDictionary<Guid, string> users)
        {
            Users = new Dictionary<Guid, string>((IDictionary<Guid, string>)users);
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder(CommandArgumentsDelimiter + Users.Count);
            foreach (var userOnline in Users)
            {
                stringBuilder.Append(CommandArgumentsDelimiter + userOnline.Key
                    + CommandArgumentsDelimiter + userOnline.Value);
            }
            return stringBuilder.ToString();
        }
    }
}
