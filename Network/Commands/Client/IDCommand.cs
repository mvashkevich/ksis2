﻿using System;

namespace Network.Commands.Client
{
    public abstract class IDCommand : BaseCommandFromServer
    {
        internal readonly Guid Id;

        public IDCommand(string id)
        {
            Id = Guid.Parse(id);
        }
        public IDCommand(Guid id)
        {
            Id = id;
        }
    }
}
