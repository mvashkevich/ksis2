﻿namespace Network.Commands.Client
{
    public abstract class BaseCommandFromServer : BaseCommand
    {
        public abstract void Execute(ICommandFromServerExecutor executor);
    }
}
