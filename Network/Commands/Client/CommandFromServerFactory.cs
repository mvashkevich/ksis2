﻿using System;

namespace Network.Commands.Client
{
    public static class CommandFromServerFactory
    {
        public static BaseCommandFromServer GetCommand(string commandText)
        {
            if (commandText == null)
            {
                throw new ArgumentNullException(nameof(commandText));
            }
            int delimiterIndex = commandText.IndexOf(BaseCommand.CommandArgumentsDelimiter, StringComparison.Ordinal);
            string commandType = commandText.Substring(0, delimiterIndex);
            string commandArguments = commandText.Substring(delimiterIndex + 1);
            switch (commandType)
            {
                case SetUserIDCommand.Type:
                    {
                        return new SetUserIDCommand(commandArguments);
                    }
                case ReceiveMessageHistoryCommand.Type:
                    {
                        return new ReceiveMessageHistoryCommand(commandArguments);
                    }
                case ReceiveMessageCommand.Type:
                    {
                        return new ReceiveMessageCommand(commandArguments);
                    }
                case UserConnectedCommand.Type:
                    {
                        return new UserConnectedCommand(commandArguments);
                    }
                case UserDisconnectedCommand.Type:
                    {
                        return new UserDisconnectedCommand(commandArguments);
                    }
                case ReceiveUsersOnlineCommand.Type:
                    {
                        return new ReceiveUsersOnlineCommand(commandArguments);
                    }
                case ReceiveUsersHistoryCommand.Type:
                    {
                        return new ReceiveUsersHistoryCommand(commandArguments);
                    }
                case LoadFileCommand.Type:
                    {
                        return new LoadFileCommand(commandArguments);
                    }
                case DeleteFileCommand.Type:
                    {
                        return new DeleteFileCommand(commandArguments);
                    }
                case GetFileCommand.Type:
                    {
                        return new GetFileCommand(commandArguments);
                    }
                case GetFileInfoCommand.Type:
                    {
                        return new GetFileInfoCommand(commandArguments);
                    }
                

            }
            throw new ArgumentException("Invalid command type: " + commandType);
        }
    }
}
