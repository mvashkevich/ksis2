﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.Client
{
    public class GetFileCommand : BaseCommandFromServer
    {
        internal const string Type = "GFIL";
        private const int ArgumentsCount = 2;
        private const int FileBase64Index = 1;
        private const int FileIDIndex = 0;

        private Guid _fileID;
        private string _fileBase64;

        public GetFileCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray(), ArgumentsCount);
            _fileID = Guid.Parse(splitArguments[FileIDIndex]);
            _fileBase64 = splitArguments[FileBase64Index];
        }

        public GetFileCommand(Guid fileID, string fileBase64)
        {
            _fileID = fileID;
            _fileBase64 = fileBase64;
        }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnGetFileCommand(_fileID, _fileBase64);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileID + CommandArgumentsDelimiter + _fileBase64;
        }
    }
}
