﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileServer;

namespace Network.Commands.Client
{
    public class LoadFileCommand : BaseCommandFromServer
    {
        internal const string Type = "LFIL";
        private const int FileIDIndex = 0;
        private const int LoadingTokenIndex = 1;

        private Guid _fileID;
        private Guid _loadingToken;

        public LoadFileCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray());
            _fileID = Guid.Parse(splitArguments[FileIDIndex]);
            _loadingToken = Guid.Parse(splitArguments[LoadingTokenIndex]);
        }

        public LoadFileCommand(Guid fileID, Guid loadingToken)
        {
            _fileID = fileID;
            _loadingToken = loadingToken;
        }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnLoadFileCommand(_loadingToken, _fileID);
        }

        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _fileID.ToString() + CommandArgumentsDelimiter + _loadingToken.ToString();
        }
    }
}
