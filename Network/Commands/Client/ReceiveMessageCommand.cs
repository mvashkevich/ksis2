﻿namespace Network.Commands.Client
{
    public class ReceiveMessageCommand : BaseMessageCommand
    {
        internal const string Type = "RECM";

        public ReceiveMessageCommand(Message message) : base(message) { }

        public ReceiveMessageCommand(string arguments) : base(arguments) { }

        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnReceiveMessageCommand(Message);
        }
        public override string ToString()
        {
            return Type + base.ToString();
        }
    }
}
