﻿using System;

namespace Network.Commands.Client
{
    public class UserConnectedCommand : BaseCommandFromServer
    {
        internal const string Type = "GETN";
        private const int ArgumentsCount = 2;
        private const int IDIndex = 0;
        private const int NameIndex = 1;
        private readonly Guid _id;
        private readonly string _name;

        public UserConnectedCommand(string arguments)
        {
            string[] splitArguments = arguments?.Split(CommandArgumentsDelimiter.ToCharArray(), ArgumentsCount);
            _id = Guid.Parse(splitArguments[IDIndex]);
            _name = splitArguments[NameIndex];
        }
        public UserConnectedCommand(Guid id, string name)
        {
            _id = id;
            _name = name;
        }
        public override string ToString()
        {
            return Type + CommandArgumentsDelimiter + _id
                + CommandArgumentsDelimiter + _name;
        }
        public override void Execute(ICommandFromServerExecutor executor)
        {
            executor?.OnUserConnectedCommand(_id, _name);
        }
    }
}
