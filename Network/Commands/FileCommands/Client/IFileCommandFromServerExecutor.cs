﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Commands.FileCommands.Client
{
    interface IFileCommandFromServerExecutor
    {
        void OnFileLoaded();
        void OnFileLoadError();
    }
}
