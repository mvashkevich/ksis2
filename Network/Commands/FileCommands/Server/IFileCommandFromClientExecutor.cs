﻿using System;
using System.IO;

namespace Network.Commands.FileCommands.Server
{
    interface IFileCommandFromClientExecutor
    {
        void OnLoadFile(TcpConnection connection, string fileName, Stream fileStream);
        void OnDeleteFile(TcpConnection connection, Guid fileID);
        void OnGetFile(TcpConnection connection, Guid fileID);
        void OnGetFileInfo(TcpConnection connection, Guid fileID);
    }
}
