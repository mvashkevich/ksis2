﻿using System.Net;
using System.Net.Sockets;

namespace Network
{
    public static class NetworkHelper
    {
        public static IPAddress GetLocalIPv4Address()
        {
            IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress address in hostEntry.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    return address;
                }
            }
            return IPAddress.Loopback;
        }
    }
}
