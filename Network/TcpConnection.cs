﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Network
{
    public sealed class TcpConnection : IDisposable
    {
        private static int SizeBufferSize = 4;
        private Socket _socket;
        private Thread _thread;
        private IPEndPoint _remoteIPEndPoint;
        private object _lock = new object();
        public bool Connected
        {
            get
            {
                if (_socket != null)
                {
                    return _socket.Connected;
                }
                return false;
            }
        }

        public event EventHandler ConnectionCreateEvent;
        public event EventHandler<ByteArrayEventArgs> ReceiveEvent;
        public event EventHandler ConnectionLostEvent;

        public TcpConnection(IPEndPoint remoteIPEndPoint)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _remoteIPEndPoint = remoteIPEndPoint;
        }

        public TcpConnection(Socket socket)
        {
            _socket = socket;
        }

        public bool Start()
        {
            bool started = true;
            if (!_socket.Connected)
            {
                try
                {
                    _socket.Connect(_remoteIPEndPoint);
                }
                catch (SocketException)
                {
                    started = false;
                }
            }
            if (_socket.Connected)
            {
                _thread = new Thread(GetMessages);
                _thread.IsBackground = true;
                _thread.Start();
                EventHandler connectionCreateEvent = ConnectionCreateEvent;
                connectionCreateEvent?.Invoke(this, EventArgs.Empty);
            }
            return started;
        }

        private void GetMessages()
        {
            bool connected = true;
            try
            {
                while (connected)
                {
                    int messageSize = GetMessageSize();
                    byte[] messageBytes;
                    connected = GetBytes(messageSize, out messageBytes);
                    if (connected)
                    {
                        var eventArgs = new ByteArrayEventArgs(messageBytes);
                        var receiveEvent = ReceiveEvent;
                        receiveEvent?.Invoke(this, eventArgs);
                    }
                }
            }
            catch (SocketException)
            {
                OnConnectionLost();
            }
            CloseSocket();
        }

        private void CloseSocket()
        {
            try
            {
                _socket.Shutdown(SocketShutdown.Both);
            }
            finally
            {
                _socket.Close();
            }
        }

        private int GetMessageSize()
        {
            byte[] sizeBuffer;
            if (GetBytes(SizeBufferSize, out sizeBuffer))
            {
                return BitConverter.ToInt32(sizeBuffer, 0);
            }
            return 0;
        }

        private bool GetBytes(int bytesCount, out byte[] bytes)
        {
            bool connected;
            bytes = null;
            var messageBuffer = new byte[bytesCount];
            int totalReceived = 0;
            do
            {
                int received = _socket.Receive(messageBuffer, totalReceived, bytesCount - totalReceived, SocketFlags.None);
                totalReceived += received;
                connected = received != 0;
            }
            while (_socket.Available > 0 && totalReceived != bytesCount);
            if (connected)
            {
                bytes = messageBuffer;
            }
            return connected;
        }

        public void SendMessage(byte[] message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }
            try
            {
                var sizeBytes = BitConverter.GetBytes(message.Length);
                lock (_lock)
                {
                    _socket.Send(sizeBytes);
                    _socket.Send(message);
                }
            }
            catch (SocketException)
            {
                OnConnectionLost();
            }
        }

        public void OnConnectionLost()
        {
            var connectionLostEvent = ConnectionLostEvent;
            connectionLostEvent?.Invoke(this, EventArgs.Empty);
            try
            {
                CloseSocket();
            }
            finally
            {
                _thread.Abort();
            }
        }

        public override string ToString()
        {
            return base.ToString() + " " + _socket.RemoteEndPoint.ToString();
        }

        public void Dispose()
        {
            _socket.Close();
        }
    }
}
