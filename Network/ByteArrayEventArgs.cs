﻿using System;

namespace Network
{
    public class ByteArrayEventArgs : EventArgs
    {
        public byte[] ByteArray
        {
            get; private set;
        }

        public ByteArrayEventArgs(byte[] byteArray)
        {
            ByteArray = byteArray;
        }
    }
}
