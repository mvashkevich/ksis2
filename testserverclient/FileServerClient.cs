﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using FileServer;
using static FileServer.FileServer;

namespace FileServerClient
{
    public static class FileServerClient  
    {
        private const string UserAgent = "File Server client";
        private static readonly HttpClient _client = new HttpClient();
        private static readonly Uri _baseUri = new Uri(Prefix);

        static async Task Main(string[] args)
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Add("User-Agent", UserAgent);

            var fileName = Console.ReadLine();
            var stream = new FileStream("input.txt", FileMode.Open);
            Guid fileID = await AddFile(stream, fileName);

            var fileStream = new FileStream("output.txt", FileMode.OpenOrCreate);
            var resultString = await GetFile(fileID);
            var bytes = Convert.FromBase64String(resultString);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Dispose();

            var fileInfo = await GetFileInfo(fileID);
            Console.WriteLine(fileInfo);

            await DeleteFile(fileID);
            Console.ReadLine();

        }

        public static async Task<Guid> AddFile(Stream fileStream, string fileName)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri(_baseUri, fileName);

            var content = new StreamContent(fileStream);
            request.Content = content;
            var response = await _client.SendAsync(request);
            var statusCode = response.StatusCode;
            if (statusCode == HttpStatusCode.Created)
            {
                var responseBytes = await response.Content.ReadAsByteArrayAsync();
                var message = System.Text.Encoding.UTF8.GetString(responseBytes);
                var fileID = JsonSerializer.Deserialize<Guid>(message);
                return fileID;
            }
            else
            {
                throw new FileExistsException(fileName);
            }
        }

        public static async Task DeleteFile(Guid fileID)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Delete;
            request.RequestUri = new Uri(_baseUri, fileID.ToString());

            var response = await _client.SendAsync(request);            
            var statusCode = response.StatusCode;
            if (statusCode != HttpStatusCode.OK)
            {
                throw new FileNotExistsException();
            }
        }

        public static async Task<string> GetFile(Guid fileID)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri(_baseUri, fileID.ToString());

            var response = await _client.SendAsync(request);
            var statusCode = response.StatusCode;
            if (statusCode != HttpStatusCode.OK)
            {
                throw new FileNotExistsException();
            }
            MemoryStream stream = (MemoryStream)await response.Content.ReadAsStreamAsync();
            var fileBase64 = Convert.ToBase64String(stream.ToArray());
            return fileBase64;
        }

        public static async Task<FileServer.FileInfo> GetFileInfo(Guid fileID)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri(_baseUri, GetFileInfoRequestIdentifier +  fileID.ToString());

            var response = await _client.SendAsync(request);
            var statusCode = response.StatusCode;
            if (statusCode != HttpStatusCode.OK)
            {
                throw new FileNotExistsException();
            }
            var fileName = response.Headers.GetValues(FileNameHeader).First();
            var fileSize = long.Parse(response.Headers.GetValues(FileSizeHeader).First());
            var fileInfo = new FileServer.FileInfo(fileID, fileName, fileSize);
            return fileInfo;
        }
    }
}
