﻿using System;
using System.Data.SQLite;

namespace FileServer.FileInfoDatabase
{
    public class FileInfoDatabaseHandler : IDisposable
    {
        private const string DataSource = "Data Source=files.db";
        private const string CreateFileInfoTableCommandText = "CREATE TABLE IF NOT EXISTS fileInfo(id BLOB PRIMARY KEY, name TEXT UNIQUE, size INTEGER);";
        private const string SelectFileInfoCommandText = "SELECT * FROM fileInfo WHERE id = @id;";
        private const string DeleteFileInfoCommandText = "DELETE FROM fileInfo WHERE id = @id;";
        private const string InsertFileInfoCommandText = "INSERT INTO fileInfo VALUES(@id, @name, @size);";
        private const string IDParameterName = "@id";
        private const string NameParameterName = "@name";
        private const string SizeParameterName = "@size";
        private const int NoRowsDeleted = 0;
        private const int IDColumnIndex = 0;
        private const int NameColumnIndex = 1;
        private const int SizeColumnIndex = 2;
        private SQLiteConnection _connection;
        private SQLiteCommand _insertFileInfoCommand;
        private SQLiteCommand _deleteFileInfoCommand;
        private SQLiteCommand _selectFileInfoCommand;

        public FileInfoDatabaseHandler()
        {
            _connection = new SQLiteConnection(DataSource);
            _connection.Open();
            using (var createFileInfoTableCommand = new SQLiteCommand(CreateFileInfoTableCommandText, _connection))
            {
                createFileInfoTableCommand.ExecuteNonQuery();
            }
        }

        public FileInfo GetFileInfo(Guid fileID)
        {
            createIDCommand(ref _selectFileInfoCommand, SelectFileInfoCommandText, fileID);
            using (SQLiteDataReader reader = _selectFileInfoCommand.ExecuteReader())
            {
                if (!reader.HasRows)
                {
                    throw new FileNotExistsException(fileID.ToString());
                }
                reader.Read();
                return new FileInfo(id: reader.GetGuid(IDColumnIndex),
                    name: reader.GetString(NameColumnIndex),
                    size: reader.GetInt64(SizeColumnIndex));
            }
        }

        public void DeleteFileInfo(Guid fileID)
        {
            createIDCommand(ref _deleteFileInfoCommand, DeleteFileInfoCommandText, fileID);
            var rowsDeleted = _deleteFileInfoCommand.ExecuteNonQuery();
            if (rowsDeleted == NoRowsDeleted)
            {
                throw new FileNotExistsException(fileID.ToString());
            }
        }

        public void AddFileInfo(FileInfo fileInfo)
        {
            if (_insertFileInfoCommand == null)
            {
                _insertFileInfoCommand = new SQLiteCommand(InsertFileInfoCommandText, _connection);
            }
            _insertFileInfoCommand.Parameters.AddWithValue(IDParameterName, fileInfo.ID);
            _insertFileInfoCommand.Parameters.AddWithValue(NameParameterName, fileInfo.Name);
            _insertFileInfoCommand.Parameters.AddWithValue(SizeParameterName, fileInfo.Size);
            _insertFileInfoCommand.ExecuteNonQuery();
        }

        private void createIDCommand(ref SQLiteCommand command, string commandText, Guid id)
        {
            if (command == null)
            {
                command = new SQLiteCommand(commandText, _connection);
            }
            command.Parameters.AddWithValue(IDParameterName, id);
        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}
