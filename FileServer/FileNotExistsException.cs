﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileServer
{
    public class FileNotExistsException : Exception
    {
        public FileNotExistsException() : base() { }

        public FileNotExistsException(string message) : base("File not exists: " + message) { }
    }
}
