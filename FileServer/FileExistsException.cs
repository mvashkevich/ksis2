﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileServer
{
    public class FileExistsException : Exception
    {
        public FileExistsException() : base() { }

        public FileExistsException(string message) : base("File exists: " + message) { }
    }
}
