﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileServer
{
    public class FileInfo
    {
        private const int ArgumentsCount = 3;
        private const int IDIndex = 0;
        private const int SizeIndex = 1;
        private const int NameIndex = 2;
        private const string Delimiter = "|";

        public Guid ID { get; private set; }
        public string Name { get; private set; }
        public long Size { get; private set; }

        public FileInfo(Guid id, string name, long size)
        {
            ID = id;
            Name = name;
            Size = size;
        }

        public FileInfo(string arguments)
        {
            string[] splitArguments = arguments?.Split(Delimiter.ToCharArray(), ArgumentsCount);
            ID = Guid.Parse(splitArguments[IDIndex]);
            Name = splitArguments[NameIndex];
            Size = int.Parse(splitArguments[SizeIndex]);
        }

        public override string ToString()
        {
            return ID.ToString() + Delimiter + Size + Delimiter + Name;
        }
    }
}
