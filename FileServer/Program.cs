﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using FileServer.FileInfoDatabase;

namespace FileServer
{
    public static class FileServer
    {
        public const string Prefix = "http://localhost:8021/";
        public const string FileNameHeader = "File-Name";
        public const string FileSizeHeader = "File-Size";
        public const string GetFileInfoRequestIdentifier = "info/";
        private const string JsonMimeType = "application/json";
        private const int AddFileUriSegmentsCount = 2;
        private const int DeleteFileUriSegmentsCount = 2;
        private const int GetFileUriSegmentsCount = 2;
        private const int GetFileInfoSegmentsCount = 3;
        private const int GetFileInfoRequestIdentifierPosition = 1;


        private static HttpListener _httpListener;
        private static FileHandler _fileHandler;

        public static async Task Main(string[] args)
        {
            _httpListener = new HttpListener();
            _httpListener.Prefixes.Add(Prefix);
            _httpListener.Start();
            _fileHandler = new FileHandler();


            while (true)
            {
                var context = await _httpListener.GetContextAsync();
                ProcessRequest(context);
            }
        }

        private static void PrintInfo(HttpListenerRequest request)
        {
            Console.Write(request.Url.LocalPath);
            Console.WriteLine(request.HttpMethod);
            Console.WriteLine("URL: {0}", request.Url.OriginalString);
            Console.WriteLine("Raw URL: {0}", request.RawUrl);
            Console.WriteLine("Query: {0}", request.QueryString);

            // Display the referring URI.
            Console.WriteLine("Referred by: {0}", request.UrlReferrer);

            //Display the HTTP method.
            Console.WriteLine("HTTP Method: {0}", request.HttpMethod);
            //Display the host information specified by the client;
            Console.WriteLine("Host name: {0}", request.UserHostName);
            Console.WriteLine("Host address: {0}", request.UserHostAddress);
            Console.WriteLine("User agent: {0}", request.UserAgent);
        }

        private static void ProcessRequest(HttpListenerContext context)
        {
            var method = context.Request.HttpMethod;
            if (method == HttpMethod.Get.Method)
            {
                OnGetRequest(context);
            }
            else if (method == HttpMethod.Post.Method)
            {
                OnPostRequest(context);
            }
            else if (method == HttpMethod.Delete.Method)
            {
                OnDeleteRequest(context);
            }
            else
            {
                OnUnsupportedMethodRequest(context);
            }
        }

        private static void OnDeleteRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            var segments = request.Url.Segments;
            if (segments.Length != DeleteFileUriSegmentsCount)
            {
                response.StatusCode = (int)HttpStatusCode.BadRequest;
                response.Close();
                return;
            }
            Guid fileID;
            try
            {
                fileID = Guid.Parse(request.Url.Segments.Last());
            }
            catch (FormatException)
            {
                response.StatusCode = (int)HttpStatusCode.NotFound;
                response.Close();
                return;
            }
            try
            {
                _fileHandler.DeleteFile(fileID);
                response.StatusCode = (int)HttpStatusCode.OK;
                Console.WriteLine("Deleted file " + " " + fileID);
            }
            catch (FileNotExistsException)
            {
                response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            response.Close();
        }

        private static void OnPostRequest(HttpListenerContext context)
        {
            OnAddFileRequest(context);
        }

        private static void OnGetRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var segments = request.Url.Segments;
            if (segments.Length == GetFileUriSegmentsCount)
            {
                OnGetFileRequest(context);
            }
            else if (segments.Length == GetFileInfoSegmentsCount
                && segments[GetFileInfoRequestIdentifierPosition] == GetFileInfoRequestIdentifier)
            {
                OnGetFileInfoRequest(context);
            }
            else
            {
                var response = context.Response;
                response.StatusCode = (int)HttpStatusCode.BadRequest;
                response.Close();
            }
        }

        private static void OnGetFileInfoRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            Guid fileID;
            bool isGuid = Guid.TryParse(request.Url.Segments.Last(), out fileID);
            if (isGuid)
            {
                FileInfo fileInfo;
                try
                {
                    fileInfo = _fileHandler.GetFileInfo(fileID);
                }
                catch (FileNotExistsException)
                {
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    response.Close();
                    return;
                }
                response.StatusCode = (int)HttpStatusCode.OK;
                response.Headers.Add(FileNameHeader, fileInfo.Name);
                response.Headers.Add(FileSizeHeader, fileInfo.Size.ToString());
                response.Close();
                Console.WriteLine("Sent file info" + " " + fileID);
            }
        }

        private static void OnGetFileRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            Guid fileID;
            bool isGuid = Guid.TryParse(request.Url.Segments.Last(), out fileID);
            if (isGuid)
            {
                Stream fileStream;
                try
                {
                    fileStream = _fileHandler.GetFile(fileID);
                }
                catch (FileNotExistsException)
                {
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    response.Close();
                    return;
                }
                response.ContentLength64 = fileStream.Length;
                var outputStream = response.OutputStream;
                response.StatusCode = (int)HttpStatusCode.OK;
                fileStream.Position = 0;
                fileStream.CopyTo(outputStream);
                outputStream.Close();
                response.Close();
                Console.WriteLine("Sent file " + " " + fileID);
            }
            else
            {
                response.StatusCode = (int)HttpStatusCode.BadRequest;
                response.Close();
            }
        }

        private static void OnAddFileRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            var segments = request.Url.Segments;
            if (segments.Length != AddFileUriSegmentsCount)
            {
                response.StatusCode = (int)HttpStatusCode.BadRequest;
                response.Close();
                return;
            }
            var fileName = segments.Last();
            try
            {
                var fileID = _fileHandler.AddFile(request.InputStream, fileName);
                response.StatusCode = (int)HttpStatusCode.Created;

                response.ContentType = JsonMimeType;
                var encoding = System.Text.Encoding.UTF8;
                response.ContentEncoding = encoding;

                var message = encoding.GetBytes(JsonSerializer.Serialize(fileID));
                response.OutputStream.Write(message, 0, message.Length);

                Console.WriteLine("Added file " + fileName + " " + fileID);
            }
            catch (FileExistsException)
            {
                response.StatusCode = (int)HttpStatusCode.Conflict;
            }
            response.Close();
        }

        private static void OnUnsupportedMethodRequest(HttpListenerContext context)
        {
            var response = context.Response;
            response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
            //// !! Add Allow header
            response.Close();
        }
    }
}
