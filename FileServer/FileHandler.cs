﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileServer.FileInfoDatabase;

namespace FileServer
{
    class FileHandler : IDisposable
    {
        private FileInfoDatabaseHandler _databaseHandler;
        private IsolatedStorageFile _storage;
        private object _lock = new object();

        /*
               using (var cmd = new SQLiteCommand("SELECT SQLITE_VERSION();", c))
               {
                   string version = cmd.ExecuteScalar().ToString();
                   Console.WriteLine($"version: {version}");

                   //cmd.CommandText = "DROP TABLE IF EXISTS fileInfo;";
                   //cmd.ExecuteNonQuery();

                   cmd.CommandText = ;
                   cmd.ExecuteNonQuery();

                   var fileID = Guid.NewGuid();
                   var fileName = "asdf";
                   var fileSize = 123;

                   cmd.CommandText = "INSERT INTO fileInfo VALUES(@id, @name, @size);";
                   cmd.Parameters.AddWithValue("@id", fileID);
                   cmd.Parameters.AddWithValue("@name", fileName);
                   cmd.Parameters.AddWithValue("@size", fileSize);
                   cmd.ExecuteNonQuery();

                   cmd.CommandText = "SELECT * FROM fileInfo;";
                   using (SQLiteDataReader rdr = cmd.ExecuteReader())
                   {

                       while (rdr.Read())
                       {
                           Console.WriteLine($"{rdr.GetGuid(0)} {rdr.GetString(1)} {rdr.GetInt32(2)}");
                           Console.WriteLine("Hello, world");

                       }
                   }
                   Console.ReadLine();

           }*/
        public FileHandler()
        {
            _databaseHandler = new FileInfoDatabaseHandler();
            _storage = IsolatedStorageFile.GetUserStoreForAssembly();
        }

        public bool FileExists(Guid fileID)
        {
            var result = true;
            try
            {
                GetFileInfo(fileID);
            }
            catch (FileNotExistsException)
            {
                result = false;
            }
            return result;
        }

        public Guid AddFile(Stream fileStream, string fileName)
        {
            lock (_lock)
            {
                if (_storage.FileExists(fileName))
                {
                    throw new FileExistsException(fileName);
                }

                long fileSize;
                using (IsolatedStorageFileStream storageFileStream = _storage.CreateFile(fileName))
                {
                    fileStream.CopyTo(storageFileStream);
                    storageFileStream.Flush();
                    fileSize = storageFileStream.Length;
                }

                var fileID = Guid.NewGuid();
                var fileInfo = new FileInfo(fileID, fileName, fileSize);
                _databaseHandler.AddFileInfo(fileInfo);

                return fileID;
            }
        }

        public Stream GetFile(Guid fileID)
        {
            lock (_lock)
            {
                var fileInfo = _databaseHandler.GetFileInfo(fileID);
                var fileStream = _storage.OpenFile(fileInfo.Name, FileMode.Open);
                var memoryStream = new MemoryStream();
                fileStream.CopyTo(memoryStream);
                fileStream.Dispose();
                return memoryStream;
            }
        }

        public FileInfo GetFileInfo(Guid fileID)
        {
            return _databaseHandler.GetFileInfo(fileID);
        }

        public void DeleteFile(Guid fileID)
        {
            lock (_lock)
            {
                var fileInfo = _databaseHandler.GetFileInfo(fileID);
                _storage.DeleteFile(fileInfo.Name);
                _databaseHandler.DeleteFileInfo(fileID);
            }
        }

        public void Dispose()
        {
            _databaseHandler.Dispose();
        }
    }
}
