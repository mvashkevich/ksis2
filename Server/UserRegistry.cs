﻿using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using Network.User;

namespace Server
{
    class UserRegistry
    {
        private Dictionary<Guid, UserInfo> _onlineUsersList;
        private Dictionary<Guid, string> _usersHistory;
        public event EventHandler<UserInfoEventArgs> UserConnectedEvent;
        public event EventHandler<UserInfoEventArgs> UserDisconnectedEvent;
        private readonly object _lock = new object();
        public UserRegistry()
        {
            _onlineUsersList = new Dictionary<Guid, UserInfo>();
            _usersHistory = new Dictionary<Guid, string>();
        }
        public bool AddUser(string name, TcpConnection connection)
        {
            if (name != "")
            {
                var id = Guid.NewGuid();
                var userInfo = new UserInfo(id, name, connection);
                lock (_lock)
                {
                    _onlineUsersList.Add(id, userInfo);
                    _usersHistory.Add(userInfo.ID, userInfo.Name);
                }
                var userConnectedEvent = UserConnectedEvent;
                var eventArgs = new UserInfoEventArgs(userInfo);
                userConnectedEvent?.Invoke(this, eventArgs);
                return true;
            }
            return false;
        }
        public UserInfo GetUserInfo(Guid id)
        {
            lock (_lock)
            {
                return _onlineUsersList[id];
            }
        }
        public void RemoveUser(Guid id)
        {
            UserInfo disconnectedUserInfo;
            lock (_lock)
            {
                disconnectedUserInfo = _onlineUsersList[id];
                _onlineUsersList.Remove(id);
            }
            var userDisconnectedEvent = UserDisconnectedEvent;
            var eventArgs = new UserInfoEventArgs(disconnectedUserInfo);
            userDisconnectedEvent?.Invoke(this, eventArgs);
        }
        public void RemoveUser(TcpConnection connection)
        {
            UserInfo disconnectedUserInfo = null;
            lock (_lock)
            {
                foreach (UserInfo userInfo in _onlineUsersList.Values)
                {
                    if (userInfo.Connection == connection)
                    {
                        disconnectedUserInfo = userInfo;
                    }
                }
            }
            if (disconnectedUserInfo != null)
            {
                RemoveUser(disconnectedUserInfo.ID);
            }
        }
        public IReadOnlyList<UserInfo> GetUsersOnline()
        {
            IReadOnlyList<UserInfo> usersOnline;
            lock (_lock)
            {
                usersOnline = _onlineUsersList.Values.ToList();
            }
            return usersOnline;
        }
        public IReadOnlyDictionary<Guid, string> GetUsersHistory()
        {
            lock (_lock)
            {
                return new Dictionary<Guid, string>(_usersHistory);
            }
        }
    }
}
