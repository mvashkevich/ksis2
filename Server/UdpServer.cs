﻿using System;
using System.Net;
using System.Net.Sockets;
using Network;

namespace Server
{
    public class UdpServer : IDisposable
    {
        public const int UdpBroadcastPort = 6000;
        private const int BufferSize = 1024;
        private const int ArrayBeginning = 0;

        private byte[] _receiveBuffer = new byte[BufferSize];
        private IPEndPoint _tcpServerEndPoint;
        private Socket _socket;
        private EndPoint _senderEndPoint;

        public UdpServer(IPEndPoint tcpServerEndPoint)
        {
            _tcpServerEndPoint = tcpServerEndPoint;
            var localEndPoint = new IPEndPoint(NetworkHelper.GetLocalIPv4Address(), UdpBroadcastPort);
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
            {
                EnableBroadcast = true
            };
            _socket.Bind(localEndPoint);
        }

        public void Dispose()
        {
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }

        public void Start()
        {
            var senderIPEndPoint = new IPEndPoint(IPAddress.None, 0);
            _senderEndPoint = senderIPEndPoint;

            _socket.BeginReceiveFrom(_receiveBuffer,
                ArrayBeginning, _receiveBuffer.Length, SocketFlags.None, ref _senderEndPoint,
                new AsyncCallback(ReceiveClientEndPointCallback), _socket);
        }

        private void ReceiveClientEndPointCallback(IAsyncResult asyncResult)
        {
            var socket = (Socket)asyncResult.AsyncState;

            int received = socket.EndReceiveFrom(asyncResult, ref _senderEndPoint);
            byte[] buffer = new byte[received];
            Array.Copy(_receiveBuffer, buffer, received);
            var clientEndPoint = BinaryEndPoint.DeserializeEndPoint(buffer, received);

            var serverEndpointBytes = BinaryEndPoint.SerializeEndPoint(_tcpServerEndPoint);

            socket.BeginSendTo(serverEndpointBytes, ArrayBeginning,
                serverEndpointBytes.Length, SocketFlags.None, clientEndPoint,
                new AsyncCallback(SendServerEndPointCallback), socket);
        }

        private void SendServerEndPointCallback(IAsyncResult asyncResult)
        {
            var socket = (Socket)asyncResult.AsyncState;
            socket.EndSendTo(asyncResult);
            try
            {
                socket.BeginReceiveFrom(_receiveBuffer,
                    ArrayBeginning, _receiveBuffer.Length, SocketFlags.None, ref _senderEndPoint,
                    new AsyncCallback(ReceiveClientEndPointCallback), socket);
            }
            catch (SocketException)
            {

            }
        }
    }
}
