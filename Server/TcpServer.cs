﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Network;
using Network.Commands;
using Network.Commands.Server;
using Network.User;
using Network.Commands.Client;
using System.IO;
using LoadFileCommand = Network.Commands.Client.LoadFileCommand;

namespace Server
{
    class TcpServer : ICommandFromClientExecutor
    {
        
        private const int AnyPort = 0;
        private const int SocketBacklog = 10;

        private UserRegistry _userRegistry;
        private Dictionary<Guid, string> _chatRegistry;
        private MessageHandler _messageHandler;
        private Socket _socket;
        private ServerFileHandler _fileHandler;

        public IPEndPoint IPEndPoint
        {
            get
            {
                return (IPEndPoint)_socket.LocalEndPoint;
            }
        }

        public TcpServer()
        {
            _userRegistry = new UserRegistry();
            _userRegistry.UserConnectedEvent += OnUserConnected;
            _userRegistry.UserDisconnectedEvent += OnUserDisconnected;

            _messageHandler = new MessageHandler();
            _messageHandler.MessageAddedEvent += OnMessageAdded;

            _chatRegistry = new Dictionary<Guid, string>();
            _chatRegistry.Add(MessageHandler.CommonChatGuid, "Common chat");

            _fileHandler = new ServerFileHandler();

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var ipEndPoint = new IPEndPoint(NetworkHelper.GetLocalIPv4Address(), AnyPort);
            _socket.Bind(ipEndPoint);
        }

        private void OnMessageAdded(object sender, MessageEventArgs e)
        {
            var message = _messageHandler.GetMessage(e.MessageID);
            var command = new ReceiveMessageCommand(message);
            if (message.ReceiverID == MessageHandler.CommonChatGuid)
            {
                foreach (UserInfo userOnline in _userRegistry.GetUsersOnline())
                {
                    SendCommand(command, userOnline.Connection);
                }
            }
            else
            {
                SendCommand(command, _userRegistry.GetUserInfo(message.ReceiverID).Connection);
                SendCommand(command, _userRegistry.GetUserInfo(message.SenderID).Connection);
            }
            Console.WriteLine("Message: " + message);
        }

        private void OnUserDisconnected(object sender, UserInfoEventArgs e)
        {
            var usersOnline = _userRegistry.GetUsersOnline();
            var disconnectedUserID = e.UserInfo.ID;
            foreach (UserInfo user in usersOnline)
            {
                var command = new UserDisconnectedCommand(disconnectedUserID);
                SendCommand(command, user.Connection);
            }
        }

        private void OnUserConnected(object sender, UserInfoEventArgs e)
        {
            var connectedUser = e.UserInfo;
            SendCommand(new SetUserIDCommand(connectedUser.ID), connectedUser.Connection);
            NotifyAboutNewUser(connectedUser);
            SendUsersAndChatsOnline(connectedUser);
            SendUsersHistory(connectedUser);
            SendMessagesHistory(connectedUser);
            Console.WriteLine(e.UserInfo);
        }

        private void SendUsersAndChatsOnline(UserInfo connectedUser)
        {
            var usersAndChats = new Dictionary<Guid, string>();
            var usersOnline = _userRegistry.GetUsersOnline();
            foreach (UserInfo user in usersOnline)
            {
                usersAndChats.Add(user.ID, user.Name);
            }
            foreach (var chat in _chatRegistry)
            {
                usersAndChats.Add(chat.Key, chat.Value);
            }
            SendCommand(new ReceiveUsersOnlineCommand(usersAndChats), connectedUser.Connection);
        }

        private void NotifyAboutNewUser(UserInfo connectedUser)
        {
            var usersOnline = _userRegistry.GetUsersOnline();
            foreach (UserInfo user in usersOnline)
            {
                if (user != connectedUser)
                {
                    var command = new UserConnectedCommand(connectedUser.ID, connectedUser.Name);
                    SendCommand(command, user.Connection);
                }
            }
        }

        private void SendMessagesHistory(UserInfo connectedUser)
        {
            var history = _messageHandler.GetMessages(connectedUser.ID, MessageHandler.CommonChatGuid);
            foreach (var message in history)
            {
                SendCommand(new ReceiveMessageHistoryCommand(message), connectedUser.Connection);
            }
        }

        private void SendUsersHistory(UserInfo connectedUser)
        {
            SendCommand(new ReceiveUsersHistoryCommand(_userRegistry.GetUsersHistory()), connectedUser.Connection);
        }

        public async Task Start()
        {
            _socket.Listen(SocketBacklog);
            while (true)
            {
                var socket = await _socket.AcceptAsync();
                var connection = new TcpConnection(socket);
                connection.ReceiveEvent += OnReceiveMessage;
                connection.ConnectionCreateEvent += OnClientConnected;
                connection.ConnectionLostEvent += OnClientDisconnected;
                connection.Start();
            }
        }

        private void OnClientDisconnected(object sender, EventArgs e)
        {
            var connection = (TcpConnection)sender;
            Console.WriteLine("Disconnected: " + connection);
            _userRegistry.RemoveUser(connection);
        }

        private void OnClientConnected(object sender, EventArgs eventArgs)
        {
            Console.WriteLine("Connected: " + sender);
        }
        
        private void OnReceiveMessage(object sender, ByteArrayEventArgs eventArgs)
        {
            var stringCommand = Encoding.Unicode.GetString(eventArgs.ByteArray);
            var command = CommandFromClientFactory.GetCommand(stringCommand);
            command.Execute(this, (TcpConnection)sender);
        }

        public void SendCommand(BaseCommand command, TcpConnection connection)
        {
            var stringCommand = command.ToString();
            connection.SendMessage(Encoding.Unicode.GetBytes(stringCommand));
        }
        
        public void OnGetIDCommand(TcpConnection connection, string name)
        {
            if (!_userRegistry.AddUser(name, connection))
            {
                SendCommand(new SetUserIDCommand(SetUserIDCommand.InvalidID), connection);
                Console.WriteLine("Invalid user");
            }
        }

        public void OnSendMessageCommand(TcpConnection connection, Guid senderID, Guid receiverID, string message, List<Guid> attachedFileIDs)
        {
            _messageHandler.AddMessage(senderID, receiverID, message, attachedFileIDs);
        }

        public void OnDisconnectCommand(TcpConnection connection, Guid senderID)
        {
            _userRegistry.RemoveUser(senderID);
        }

        public async Task OnLoadFile(TcpConnection connection, Guid loadingToken, string fileName, Stream fileStream)
        {
            var fileID = await _fileHandler.AddFile(fileName, fileStream);
            SendCommand(new LoadFileCommand(fileID, loadingToken), connection);
        }

        public async Task OnDeleteFile(TcpConnection connection, Guid fileID)
        {
            await _fileHandler.DeleteFile(fileID);
            SendCommand(new Network.Commands.Client.DeleteFileCommand(fileID), connection);
        }

        public async Task OnGetFile(TcpConnection connection, Guid fileID)
        {
            var fileBase64 = await _fileHandler.GetFile(fileID);
            SendCommand(new Network.Commands.Client.GetFileCommand(fileID, fileBase64), connection);
        }

        public async Task OnGetFileInfo(TcpConnection connection, Guid fileID)
        {
            var fileInfo = await _fileHandler.GetFileInfo(fileID);
            SendCommand(new Network.Commands.Client.GetFileInfoCommand(fileInfo), connection);
        }
    }
}
