﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new TcpServer();
            server.Start();
            var udpBroadcaster = new UdpServer(server.IPEndPoint);
            udpBroadcaster.Start();
            Console.WriteLine("Started " + server.IPEndPoint);
            Console.ReadLine();
        }
    }
}
