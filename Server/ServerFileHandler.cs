﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileServerClient;
using FileServer;

namespace Server
{
    class ServerFileHandler
    {
        private Dictionary<Guid, ServerFileInfo> _fileInfos = new Dictionary<Guid, ServerFileInfo>();

        public static Guid ErrorGuid = Guid.Empty;
        public static string ErrorString = String.Empty;

        public async Task<Guid> AddFile(string fileName, Stream fileStream)
        {
            var serverFileInfo = new ServerFileInfo();
            serverFileInfo.fileName = fileName;
            var serverFileID = Guid.NewGuid();
            serverFileInfo.ServerFileID = serverFileID;
            Guid fileStorageFileID;
            try
            {
                fileStorageFileID = await FileServerClient.FileServerClient.AddFile(fileStream, serverFileID.ToString());
            }
            catch (FileExistsException)
            {
                return ErrorGuid;
            }
            serverFileInfo.FileStorageFileID = fileStorageFileID;

            _fileInfos.Add(serverFileID, serverFileInfo);
            return serverFileID;
        }

        public async Task DeleteFile(Guid serverFileID)
        {
            if (_fileInfos.ContainsKey(serverFileID))
            {
                var fileStorageFileID = _fileInfos[serverFileID].FileStorageFileID;
                await FileServerClient.FileServerClient.DeleteFile(fileStorageFileID);
                _fileInfos.Remove(serverFileID);
            }
        }

        public async Task<string> GetFile(Guid serverFileID)
        {
            if (_fileInfos.ContainsKey(serverFileID))
            {
                var fileStorageFileID = _fileInfos[serverFileID].FileStorageFileID;
                return await FileServerClient.FileServerClient.GetFile(fileStorageFileID);
            }
            return ErrorString;
        }

        public async Task<FileServer.FileInfo> GetFileInfo(Guid serverFileID)
        {
            FileServer.FileInfo fileInfo = null;
            if (_fileInfos.ContainsKey(serverFileID))
            {
                var serverFileInfo = _fileInfos[serverFileID];
                var fileStorageFileID = serverFileInfo.FileStorageFileID;
                var fileName = serverFileInfo.fileName;
                var fileStorageFileInfo = await FileServerClient.FileServerClient.GetFileInfo(fileStorageFileID);
                var fileSize = fileStorageFileInfo.Size;
                fileInfo = new FileServer.FileInfo(serverFileID, fileName, fileSize);
            }
            return fileInfo;
        }
    }
}
