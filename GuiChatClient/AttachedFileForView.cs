﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatClient;

namespace GuiChatClient
{
    public class AttachedFileForView : INotifyPropertyChanged
    {
        private static readonly string[] SizeUnits = { "b", "KB", "MB", "GB" };
        private const int ConvertBase = 1024;
        public Guid ID;

        public FileStatus Status { get => DownloadManager.GetFileStatus(ID); }

        public string FileName { get; set; }
        public string Size { get {
                int sizeUnitIndex = (int)Math.Log(_size, ConvertBase);
                long convertedSize = _size / (1L << (sizeUnitIndex * 10));
                return convertedSize.ToString() + " " + SizeUnits[sizeUnitIndex];
            } }

        private long _size;

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnFileStatusChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Status)));
        }

        public AttachedFileForView(Guid id, string fileName, long size)
        {
            ID = id;
            FileName = fileName;
            _size = size;
        }
    }
}
