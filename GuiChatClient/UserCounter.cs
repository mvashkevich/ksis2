﻿using System.ComponentModel;

namespace GuiChatClient
{
    public class UserCounter : INotifyPropertyChanged
    {
        private int _count;
        public int Count
        {
            get => _count; set
            {
                _count = value;
                NotifyPropertyChanged(nameof(Count));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            propertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
