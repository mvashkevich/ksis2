﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileServer;

namespace GuiChatClient
{
    public class Message
    {
        public string DateTime { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public ObservableCollection<AttachedFileForView> AttachedFiles { get; set; }
    }
}
