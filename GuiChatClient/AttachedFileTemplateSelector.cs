﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GuiChatClient
{
    public class AttachedFileTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            if (element != null && item != null && item is AttachedFileForView)
            {
                AttachedFileForView file = item as AttachedFileForView;

                string resourceName = "";
                switch (file.Status)
                {
                    case ChatClient.FileStatus.Downloaded:
                        {
                            resourceName = "Downloaded";
                            break;
                        }
                    case ChatClient.FileStatus.Downloading:
                        {
                            resourceName = "Downloading";
                            break;
                        }
                    case ChatClient.FileStatus.NotDownloaded:
                        {
                            resourceName = "NotDownloaded";
                            break;
                        }
                }
                return element.FindResource(resourceName) as DataTemplate;
            }
            return null;
        }
    }
}
