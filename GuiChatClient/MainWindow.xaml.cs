﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Network;
using ChatClient;
using Network.Files;
using System.Linq;
using System.Collections.Generic;

namespace GuiChatClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string InvalidSizeMessage = $"Превышен максимальный размер файла. Поддерживаются файлы размером не более {FileHelper.MaxFileSize / (1024 * 1024) } Мб";
        private string InvalidTotalSizeMessage = $"Превышен суммарный размер всех файлов. Поддерживаемый суммарный размер не более {FileHelper.MaxTotalSize / (1024 * 1024) } Мб";
        private string InvalidFileExtensionMessage = "Расширение файла не поддерживается";
        private string FileLoadErrorCaption = "Ошибка при загрузке файла";
        private string FileLoadingMessage = "Дождитесь окончания загрузки файлов";
        private const int DeltaCommonChatAndSelf = 2;
        private TcpClient _tcpClient;
        private Dispatcher _dispatcher;
        private Guid _selectedDialogID;
        private object _messagesLock = new object();
        private object _dialogsLock = new object();
        private Dictionary<Guid, AttachedFileForView> _guidToFileForView = new Dictionary<Guid, AttachedFileForView>();
        public ObservableCollection<Message> Messages = new ObservableCollection<Message>();
        public ObservableCollection<Dialog> Dialogs = new ObservableCollection<Dialog>();
        public ObservableCollection<FileServer.FileInfo> AttachedFiles = new ObservableCollection<FileServer.FileInfo>();
        public UserCounter UserCounter = new UserCounter();

        public MainWindow()
        {
            InitializeComponent();
            lvMessages.ItemsSource = Messages;
            lvDialogs.ItemsSource = Dialogs;
            lvDialogs.SelectionChanged += OnDialogSelected;
            Loaded += MainWindow_Loaded;
            Closing += MainWindow_Closing;
            btnSend.Click += SendMessage;
            tbUsersOnlineCount.DataContext = UserCounter;
            _dispatcher = Dispatcher.CurrentDispatcher;
        }

        private void OnDialogSelected(object sender, SelectionChangedEventArgs e)
        {
            lock (_messagesLock)
            {
                if (e.AddedItems.Count == 1)
                {
                    if (_tcpClient.LoadingFiles.Count == 0)
                    {
                        var dialog = ((Dialog)e.AddedItems[0]);
                        dialog.Message = "";
                        _selectedDialogID = dialog.ID;
                        ShowMessages();
                    }
                    else
                    {
                        MessageBox.Show(FileLoadingMessage);
                    }
                }
            }
        }

        private void ShowMessages()
        {
            if (_selectedDialogID != Guid.Empty)
            {
                Messages.Clear();
                foreach (var message in _tcpClient.GetMessages(_selectedDialogID))
                {
                    Messages.Add(GetMessageForView(message));
                }
                AttachedFiles.Clear();
                foreach (var attachedFileInfo in _tcpClient.AttachedFiles[_selectedDialogID].AttachedFiles.Values)
                {
                    AttachedFiles.Add(attachedFileInfo);
                }
            }
        }

        private Message GetMessageForView(Network.Message message)
        {
            var attachedFiles = new ObservableCollection<AttachedFileForView>();
            foreach (var fileID in message.AttachedFilesIDs)
            {
                if (_tcpClient.ExistingFiles.ContainsKey(fileID))
                {
                    var fileInfo = _tcpClient.ExistingFiles[fileID];
                    var fileInfoForView = new AttachedFileForView(fileID, fileInfo.Name, fileInfo.Size);
                    _guidToFileForView[fileID] = fileInfoForView;
                    attachedFiles.Add(fileInfoForView);
                }
            }
            return new Message()
            {
                DateTime = message.DateTime.ToString(),
                Name = _tcpClient.UsersHistory[message.SenderID],
                Text = message.MessageText,
                AttachedFiles = attachedFiles
            };
        }

        private void SendMessage(object sender, RoutedEventArgs e)
        {
            if (_selectedDialogID != Guid.Empty)
            {
                if (_tcpClient.LoadingFiles.Count == 0)
                {
                    _tcpClient.SendMessage(_selectedDialogID, tbMessage.Text);
                    tbMessage.Text = "";
                    AttachedFiles.Clear();
                }
                else
                {
                    MessageBox.Show(FileLoadingMessage);
                }
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _tcpClient?.Stop();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            IPEndPoint ipEndPoint;
            do
            {
                ipEndPoint = UdpClient.GetServerEndPoint();
                if (ipEndPoint == null)
                {
                    MessageBoxResult result = MessageBox.Show("Сервер не найден. Повторить попытку?", "Сервер не найден", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result != MessageBoxResult.Yes)
                    {
                        return;
                    }
                }
            }
            while (ipEndPoint == null);
            var userNameDialog = new UserNameWindow();
            if (userNameDialog.ShowDialog() == true)
            {
                _tcpClient = new TcpClient();
                _tcpClient.ServerDisconnectedEvent += OnServerDisconnected;
                _tcpClient.UserConnectedEvent += OnUserConnected;
                _tcpClient.UserDisconnectedEvent += OnUserDisconnected;
                _tcpClient.MessageReceivedEvent += OnMessageReceived;
                _tcpClient.UsersOnlineReceivedEvent += OnUsersOnlineReceived;
                _tcpClient.HistoryReceivedEvent += OnHistoryReceived;
                _tcpClient.AttachFileResultEvent += OnFileAddResult;
                lvAttachedFiles.ItemsSource = AttachedFiles;
                _tcpClient.FileInfoReceivedEvent += OnFileInfoReceived;
                DownloadManager.DownloadStatusChangedEvent += OnDownloadStatusChanged;
                _tcpClient.Start(ipEndPoint, userNameDialog.UserName);
            }
            else
            {
                Close();
            }
        }

        private void OnDownloadStatusChanged(object sender, DownloadStatusChangedEventArgs e)
        {
            _guidToFileForView[e.FileID].OnFileStatusChanged();
        }

        private void OnFileInfoReceived(object sender, EventArgs e)
        {
            Action action = () =>
            {
                ShowMessages();
            };
            _dispatcher.Invoke(action);
        }

        private void OnFileAddResult(object sender, FileAddResultEventArgs e)
        {
            switch (e.Result)
            {
                case AttachFileResult.FileSizeLimit:
                    {
                        MessageBox.Show(InvalidSizeMessage, FileLoadErrorCaption, MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    }
                case AttachFileResult.TotalFileSizeLimit:
                    {
                        MessageBox.Show(InvalidTotalSizeMessage, FileLoadErrorCaption, MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    }
                case AttachFileResult.InvalidExtension:
                    {
                        MessageBox.Show(InvalidFileExtensionMessage, FileLoadErrorCaption, MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    }
                case AttachFileResult.Ok:
                    {
                        Action action = () =>
                        {
                            AttachedFiles.Clear();
                            foreach (var attachedFileInfo in _tcpClient.AttachedFiles[_selectedDialogID].AttachedFiles.Values)
                            {
                                AttachedFiles.Add(attachedFileInfo);
                            }
                        };
                        _dispatcher.Invoke(action);
                        break;
                    }
            }
        }

        private void OnHistoryReceived(object sender, MessageEventArgs e)
        {
            Action<Guid> action = (messageID) =>
                {
                    ShowMessages();
                };
            _dispatcher.Invoke(action, e.MessageID);
        }

        private void OnUsersOnlineReceived(object sender, EventArgs e)
        {
            Action action = () =>
            {
                foreach (var dialog in _tcpClient.UsersOnline)
                {
                    if (dialog.Key != _tcpClient.ID)
                    {
                        lock (_dialogsLock)
                        {
                            Dialogs.Add(new Dialog() { ID = dialog.Key, Name = dialog.Value, Message = "" });
                        }
                    }
                }
                UserCounter.Count = _tcpClient.UsersOnline.Count - DeltaCommonChatAndSelf;
            };
            _dispatcher.Invoke(action);
        }

        private void OnMessageReceived(object sender, Network.MessageEventArgs e)
        {
            Action<Guid> action = (messageID) =>
            {
                var message = _tcpClient.GetMessage(messageID);
                if (((message.ReceiverID != MessageHandler.CommonChatGuid) && (message.ReceiverID == _selectedDialogID || message.SenderID == _selectedDialogID)) || ((message.ReceiverID == MessageHandler.CommonChatGuid) && (_selectedDialogID == MessageHandler.CommonChatGuid)))
                {
                    lock (_messagesLock)
                    {
                        Messages.Add(GetMessageForView(message));
                    }
                }
                else
                {
                    foreach (Dialog dialog in Dialogs)
                    {
                        if (((message.ReceiverID == MessageHandler.CommonChatGuid)
                            && (dialog.ID == MessageHandler.CommonChatGuid))
                            || ((message.ReceiverID != MessageHandler.CommonChatGuid)
                            && (dialog.ID == message.SenderID)))
                        {
                            dialog.Message = "Новое сообщение";
                            break;
                        }
                    }
                }
            };
            _dispatcher.Invoke(action, e.MessageID);
        }

        private void OnUserDisconnected(object sender, ClientUserInfoEventArgs e)
        {
            Action<Guid> action = (id) =>
            {
                lock (_dialogsLock)
                {
                    Dialog dialogToRemove = null;
                    foreach (Dialog dialog in Dialogs)
                    {
                        if (dialog.ID == id)
                        {
                            dialogToRemove = dialog;
                            break;
                        }
                    }
                    Dialogs.Remove(dialogToRemove);
                }
                UserCounter.Count--;
            };
            _dispatcher.Invoke(action, e.ID);
        }

        private void OnUserConnected(object sender, ClientUserInfoEventArgs e)
        {
            var dialog = new Dialog() { ID = e.ID, Name = e.Name, Message = "" };
            Action<Dialog> action = (d) =>
            {
                lock (_dialogsLock)
                {
                    Dialogs.Add(d);
                }
                UserCounter.Count++;
            };
            _dispatcher.Invoke(action, dialog);
        }

        private void OnServerDisconnected(object sender, EventArgs e)
        {
            Action action = () =>
            {
                MessageBox.Show("Соединение с сервером утеряно", "Соединение утеряно", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            };
            _dispatcher.Invoke(action);
        }

        private void DockPanel_Drop(object sender, DragEventArgs e)
        {
            if (_selectedDialogID != Guid.Empty)
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (var file in files)
                {
                    _tcpClient.AddFile(_selectedDialogID, file);
                }
            }
        }

        private void OnDeleteFile(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            Guid fileID = (Guid)button.Tag;
            _tcpClient.UnattachFile(_selectedDialogID, fileID);

        }


        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView listView = (ListView)sender;

            listView.SelectedItem = null;

            if (e.AddedItems.Count == 1)
            {
                var addedItem = (AttachedFileForView)e.AddedItems[0];

                _tcpClient.HandleFileClick(addedItem.ID);
            }
        }
    }
}
