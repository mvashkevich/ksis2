﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiChatClient
{
    public class Dialog : INotifyPropertyChanged
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        private string _message;
        public string Message
        {
            get => _message; set
            {
                _message = value; NotifyPropertyChanged(nameof(Message));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            var propertyChanged = PropertyChanged;
            propertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
